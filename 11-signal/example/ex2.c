#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

void handler (int signum) { 
	printf("Sygnał %d\n", signum);
}


int main(int argc, char **argv) {
	struct sigaction new_action, old_action;
	new_action.sa_handler = handler;
	sigemptyset (&new_action.sa_mask);
	new_action.sa_flags = 0;
	sigaction (SIGUSR1, &new_action, &old_action);
	for (;;) {
	sleep(1);
	}
	return 0;
}
