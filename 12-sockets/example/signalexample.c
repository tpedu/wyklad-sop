#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

volatile sig_atomic_t x = 0;
void obsluga() {
    switch (x) {
    case 0:
        printf("dostalem synal TERM :D\n");
        break;
    case 1:
        printf("ej\n");
        break;
    case 2:
        printf("serio?\n");
        break;
    case 3:
        printf("niee :(\n");
        break;
    }
    x++;
}

int main(int argc, char **argv) {
    printf("%d %d\n", getpid(), getppid());
    signal(SIGTERM, obsluga);
    while (x < 4) {
        sleep (3);
        printf("jeszcze dzialam\n");
    }

    return 0;
}
