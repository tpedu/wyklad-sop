/**

(C) 2015 Tadeusz Puźniakowski

You shoud have received License along with this code. If not, then you are not allowed to use this code.

*/

// #define WIRING

#include "web.hpp"

#include <iostream>
#include <fstream>
#include <unistd.h>
#include <chrono>
#include <queue>
#include <thread>
#include <mutex>
#include <assert.h>
#include <cmath>
#include <condition_variable>
#include <utility>

#include <sys/sem.h>

using namespace tp;

int semid;

void dosignal(int id) { // operacja V
    struct sembuf operations[1];
    operations[0].sem_num = 0; // numer semafora
    operations[0].sem_op = 1; // dodanie 1 do semafora
    operations[0].sem_flg = 0; // oczekiwanie
    semop(id, operations, 1);
}

void dowait(int id) { // operacja P
    struct sembuf operations[1];
    operations[0].sem_num = 0; // numer semafora
    operations[0].sem_op = -1; // odejmujemy 1 od semafora
    operations[0].sem_flg = 0; // oczekiwanie
    semop(id, operations, 1);
}




int onGcode(WebService::Request &req) {
    std::cout << "onGcode..\n";
    dowait(semid);
    std::cout << "  --> onGcode..\n";
    for (auto& x: req.getArguments()) {
        std::cout << " " << x.first << "='" << x.second << "' ";
    }
    if (req.getPath().size() > 1) {
        std::cout << "tstep" << req.getPath()[1] << ";\n";
        req.setResponse("{\"status\":\"OK\"}");
    } else {
        req.setResponse("{\"err\":\"!! Malformed query. Shoudl be something like /g/G0X1Y1Z0\"}");
    }
    req.setMime("application/vnd.api+json");
    sleep(2);
    std::cout << "      onGcode.. -->\n";
    dosignal(semid);
    return 200;
}

void onDone(WebService::Request &req) {
    std::cout << "OK" << std::endl;
}
void onError(WebService::Request &req) {
    std::cout << "error!!" << std::endl;
}

volatile sig_atomic_t finishMainLoop = 0;
void sigtermhandle() {
    finishMainLoop = 1;
}
int main(int argc, char **argv) {
    key_t semkey = ftok(argv[0],1988);
    semid = semget(semkey, 1, 0600 | IPC_CREAT);
    semctl(semid, 0, SETVAL, 1); // ustawmy na poczatku 1
    WebService srv(8080);
    std::string line;
    srv.addHandler("g",onGcode);
    signal(SIGTERM, (sighandler_t) sigtermhandle);
    signal(SIGINT, (sighandler_t) sigtermhandle);
    while(finishMainLoop == 0) {
        srv.handleConnection(onDone,onError);
    }
    std::cout << "finished webservice\n";
    semctl (semid,0,IPC_RMID,0);

    return 0;
}
