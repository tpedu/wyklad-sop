/**

(C) 2015 Tadeusz Puźniakowski

You shoud have received License along with this code. If not, then you are not allowed to use this code.

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>
#include <pwd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <grp.h>
#include <sys/types.h>
#include <dirent.h>
#include <arpa/inet.h>

#include <string>
#include <vector>

#include <sys/time.h>
#include <sstream>
#include <iostream>

#include <thread>
#include <mutex>
#include <algorithm>


#include "web.hpp"

namespace tp {


// after http://stackoverflow.com/questions/236129/split-a-string-in-c
std::vector<std::string> &split(const std::string &s, const char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}
// after http://stackoverflow.com/questions/236129/split-a-string-in-c
std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

// after http://stackoverflow.com/
std::string trim(const std::string& str,
                 const std::string& whitespace)
{
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return ""; // no content
    const auto strEnd = str.find_last_not_of(whitespace);
    const auto strRange = strEnd - strBegin + 1;
    return str.substr(strBegin, strRange);
}

#define BACKLOG 10




/**
 * Konwersja uriencoded string na ciag znakow
 * */
std::string uridecode(const std::string uristr) {
    std::string retstr;
    for (size_t i = 0; i < uristr.length(); i++) {
        if (uristr[i] == '%' && i < (uristr.length()-2)) {
            std::string num = "";
            num = num + ((char)uristr[i+1]) + ((char)uristr[i+2]);
            char c = strtoul(num.substr(0, 2).c_str(), NULL, 16);
            retstr = retstr + c;
            i+= 2;
        } else if (uristr[i] == '+') {
            retstr = retstr + ' ';
        } else {
            retstr = retstr + uristr[i];
        }
    }
    return retstr;
}

std::vector<std::string> WebService::Request::getPath() const {
    std::vector<std::string> pp = split(getQueryString(),'?');
    std::string pp0 = (pp.size()>0)?pp[0]:"";
    std::vector<std::string> p = split(pp0,'/');
    std::vector<std::string> ret;
    if (p.size() > 1) p.erase(p.begin());
    for (size_t i = 0; i < p.size(); i++) {
        ret.push_back(uridecode(p[i]));
    }
    return ret;
};
std::map<std::string,std::string> WebService::Request::getArguments() const {
    std::map<std::string,std::string> ret;
    std::vector<std::string> r = split(getQueryString(),'?');
    if (r.size() <= 1) return ret;
    std::vector<std::string> p = split(r[1],'&');
    for (size_t i = 0; i < p.size(); i++) {
        size_t j = 0;
        std::string key,value;
        while (j < p[i].length()) {
            if (p[i][j] != '=') {
                key = key + p[i][j];
            } else {
                j++;
                break;
            }
            j++;
        }
        while (j < p[i].length()) {
            value = value + p[i][j];
            j++;
        }
        ret[key] = uridecode(value);
        //ret.push_back(WebService::uridecode(p[i]));
    }
    return ret;
};
std::vector<std::string> WebService::Request::getCookies() const {
    std::vector<std::string> p = split(cookiesString,' ');
    if (p.size() > 1) p.erase(p.begin());
    return p;
};
std::string WebService::Request::getMethod() const {
    return method;
};

std::string WebService::Request::getQueryString() const {
    return queryString;
};

std::string WebService::Request::getProto() const {
    return proto;
};

std::string WebService::Request::getHostname() const {
    return hostname;
};

void WebService::Request::setResponse(const std::string r) {
    response = r;
};
void WebService::Request::setMime(const std::string m) {
    mime = m;
};
std::string WebService::Request::getMime() const {
    /*
    "text/html";
    "application/vnd.api+json";
    "application/octet-stream";
    */
    return mime;
}

int WebService::defaultOnRequestHandle(WebService::Request &req) {
    req.setResponse("NOT FOUND " + (req.getPath()[0]) + "\n");
    req.setMime("text/html");
    return 200;
}


void WebService::sigchld_handler(int s)
{
    while(waitpid(-1, NULL, WNOHANG) > 0) sleep(1);
}


void WebService::asyncTask(int new_fd, struct sockaddr_in their_addr, int reqN, void (*onDone)(Request &), void (*onError)(Request &)) {
    char *in;
    std::string sent;
    std::string retCode("200 - ok");
    Request requ;
    requ.id = reqN;
    in = new char [5000000]; // obsluga zapytan do rozmiaru okolo 5 MB

    if (read(new_fd, in, 5000000) == -1) {
        perror("recive");
        close(new_fd);
        delete [] in;
        if (onError != NULL) {
            onError(requ);
        }
        return ;
    } else {
        std::vector<std::string> lines = split(in, '\r');
        std::stringstream ss(trim(lines[0]));
        ss >> requ.method >> requ.queryString >> requ.proto;
        for (size_t i = 1; i < lines.size(); i++) {
            std::stringstream ss(trim(lines[i]));
            std::string header;
            ss >> header;
            if (header == std::string("Host:")) {
                ss >> requ.hostname;
            } else if (header == std::string("Cookie:")) {
                requ.cookiesString = trim(lines[i]);
            }
        }
        // OBSLUGA ZAPYTANIA -- to nalezy ustawic
        int rv = 0;
        for (auto& x: requestHandlers) {
            int (*orh)(Request &) = x.second;
            if (x.first == requ.getPath()[0]) {
                retCode = std::to_string(orh(requ)); // faktyczny handler
                rv ++;
                break;
            }
        }
        if (rv == 0) {
            // obsluga domyslnego uchwytu
            onRequestHandle(requ);
        }

        requ.setMime("text/html");

        sent = "HTTP/1.1 "
               + retCode
               + "\nServer: frezia-webservice\n"
               + "Content-Length: "
               + std::to_string(requ.response.length())
               + "\nConnection: close\nContent-Type: "
               + requ.getMime()
               + "; charset="
               + charset
               // TODO: Dodanie sesji i ciasteczek
               //+ "\nSet-Cookie: theme=light\n"
               + "\n\n";
        send(new_fd, sent.c_str(), sent.length(), MSG_NOSIGNAL);
        send(new_fd, requ.response.c_str(), requ.response.length(), MSG_NOSIGNAL);
        close(new_fd);
        delete [] in;
        if (onDone != NULL) {
            onDone(requ);
        }
    }
}

int WebService::handleConnection(void (*onDone)(Request &), void (*onError)(Request &)) {
    int s;
    struct sockaddr_in remote;
    socklen_t sin_size;
    sin_size = sizeof(struct sockaddr_in);
    if ((s = accept(sockfd, (struct sockaddr *)&remote, &sin_size)) == -1) {
        perror("on error");
        return -1;
    } else {
        reqCounter++;
        // std::cout <<
        // ((remote.sin_addr.s_addr>>0) & 0x0ff) << "." <<
        // ((remote.sin_addr.s_addr>>8) & 0x0ff) << "." <<
        // ((remote.sin_addr.s_addr>>16) & 0x0ff) << "." <<
        // ((remote.sin_addr.s_addr>>24) & 0x0ff) << " : " <<
        // remote.sin_addr.s_addr << "\n";
        // if (16777343 == remote.sin_addr.s_addr) std::cout << "Local connection\n";
        // http://en.cppreference.com/w/cpp/language/lambda
        std::thread([&]() {
            asyncTask(s, remote, reqCounter, onDone, onError);
        }).detach(); // odpalenie zadania

        // jesli nie rownolegle: asyncTask(s, their_addr, reqCounter, onDone, onError);
        return 0;
    }
}

void WebService::addHandler(std::string addr, int (*onRequestHandle)(Request &)) {
    requestHandlers[addr] = onRequestHandle;
}
void WebService::removeHandler(std::string addr) {
    requestHandlers.erase(addr);
}

WebService::WebService (const int port ) {
    struct sigaction sa;

    this->port = port;
    reqCounter = 1;
    charset = "utf8";
    int yes=1;
    struct sockaddr_in my_addr;

    if ((sockfd = socket(PF_INET, SOCK_STREAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }

    if (setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int)) == -1) {
        perror("setsockopt");
        exit(1);
    }

    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(port);
    my_addr.sin_addr.s_addr = INADDR_ANY;
    memset(&(my_addr.sin_zero), '\0', 8);

    if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) == -1) {
        perror("bind");
        exit(1);
    }

    if (listen(sockfd, BACKLOG) == -1) {
        perror("listen");
        exit(1);
    }

    sa.sa_handler = WebService::sigchld_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGCHLD, &sa, NULL) == -1) {
        perror("sigaction");
        exit(1);
    }
    signal(SIGPIPE, SIG_IGN);
    onRequestHandle = WebService::defaultOnRequestHandle;
    std::cout << "started webapp on port " << port << "\n";
}

WebService::~WebService() {
    close(sockfd);
}

}
