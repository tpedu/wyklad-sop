#ifndef __WEB_HPP__
#define __WEB_HPP__
/**

(C) 2015 Tadeusz Puźniakowski

You shoud have received License along with this code. If not, then you are not allowed to use this code.

*/
//#include "config.hpp"

#include <string>
#include <vector>

#include <sys/time.h>
#include <sstream>

#include <thread>
#include <mutex>
#include <algorithm>
#include <map>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>
#include <pwd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <grp.h>
#include <sys/types.h>
#include <dirent.h>

#define BACKLOG 10


namespace tp {

/**
 * Konwersja uriencoded string na ciag znakow
 * */
std::string uridecode(const std::string uristr);
// after http://stackoverflow.com/questions/236129/split-a-string-in-c
std::vector<std::string> &split(const std::string &s, const char delim, std::vector<std::string> &elems);
// after http://stackoverflow.com/questions/236129/split-a-string-in-c
std::vector<std::string> split(const std::string &s, char delim);
// after http://stackoverflow.com/
std::string trim(const std::string& str,
                 const std::string& whitespace = "\n \t\r");


typedef class WebService {
protected:
    //Sockets
    int sockfd;
    int reqCounter;

public:
    typedef class Request {
    public:
        int id;

        std::string method;
        std::string queryString;
        std::string proto;
        std::string hostname;
        std::string response;
        std::string cookiesString;
        std::string mime;

        std::vector<std::string> getPath() const ;
        std::map<std::string,std::string> getArguments() const ;
        std::vector<std::string> getCookies() const ;
        std::string getMethod() const ;
        std::string getQueryString() const ;

        std::string getProto() const ;
        std::string getHostname() const;
        void setResponse(const std::string r);
        void setMime(const std::string m);
        std::string getMime() const ;
    } Request;

    static int defaultOnRequestHandle(WebService::Request &req);

    int (*onRequestHandle)(Request &requ);

    std::map<std::string,int (*)(Request &requ)> requestHandlers;

    std::string charset;
    int port;

    static void sigchld_handler(int s);

    int handleConnection(void (*onDone)(Request &) = NULL, void (*onError)(Request &) = NULL);
    void asyncTask(int new_fd, struct sockaddr_in their_addr, int reqN,
                   void (*onDone)(Request &requ) = NULL, void (*onError)(Request &) = NULL);


    void addHandler(std::string addr, int (*onRequestHandle)(Request &));
    void removeHandler(std::string addr);

    WebService (const int port );
    virtual ~WebService();

} WebService;

}

#endif
