

















Co może zawierać blok kontrolny procesu?
Co może zawierać blok kontrolny procesu?
Co należy wykonać aby program nasłuchiwał na wybranym porcie w przypadku protokołu TCP/IP?
Co należy wykonać aby program nasłuchiwał na wybranym porcie w przypadku protokołu TCP/IP?
Co się dzieje podczas przełączania kontekstu?
Co się dzieje w momencie linkowania (w kontekście kompilacji programu do kodu wykonywalnego)?
Co się dzieje w momencie linkowania (w kontekście kompilacji programu do kodu wykonywalnego)?
Co się składa na proces?
Co się składa na proces?
Co się stanie, jeśli system będzie wykonywał zbyt wiele wymagających dużo pamięci procesów? Jak się nazywa zjawisko, które wtedy się pojawi?
Co się stanie, jeśli system będzie wykonywał zbyt wiele wymagających dużo pamięci procesów? Jak się nazywa zjawisko, które wtedy się pojawi?
Co to jest adres bezwzględny?
Co to jest adres bezwzględny?
Co to jest adres względny? Co on tworzy?
Co to jest adres względny? Co on tworzy?
Co to jest biblioteka współdzielona?
Co to jest biblioteka współdzielona?
Co to jest język skryptowy?
Co to jest język skryptowy?
Co to jest mutex?
Co to jest mutex?
Co to jest pamięć współdzielona (shared memory)?
Co to jest pamięć współdzielona (shared memory)?
Co to jest PID?
Co to jest PID?
Co to jest powłoka systemu?
Co to jest powłoka systemu?
Co to jest PPID?
Co to jest PPID?
Co to jest segmentacja?
Co to jest segmentacja?
Co to jest semafor?
Co to jest semafor?
Co to jest stronicowanie?
Co to jest stronicowanie?
Co to jest stronicowanie na żądanie?
Co to jest stronicowanie na żądanie?
Co to jest system operacyjny?
Co to jest system operacyjny?
Co to jest sytuacja zagłodzenia (w kontekście systemów operacyjnych)?
Co to jest sytuacja zagłodzenia (w kontekście systemów operacyjnych)?
Co to jest zakleszczenie? Podaj przykład.
Co to jest zakleszczenie? Podaj przykład.
Co to jest zmienna powłoki i do czego może służyć?
Co to jest zmienna powłoki i do czego może służyć?
Co to znaczy ,,proces ograniczony przez procesor''?
Co to znaczy ,,proces ograniczony przez procesor''?
Co to znaczy ,,proces ograniczony przez wejście-wyjście''?
Co to znaczy ,,proces ograniczony przez wejście-wyjście''?
Co to znaczy ,,wyekspediowanie procesu'' (process dispatch)?
Co to znaczy ,,wyekspediowanie procesu'' (process dispatch)?
Co trzeba zrobić, aby zablokować wyłączenie programu konsolowego w momencie wciśnięcia Ctrl+C?
Co trzeba zrobić, aby zablokować wyłączenie programu konsolowego w momencie wciśnięcia Ctrl+C?
Czy kolejka nazwana FIFO (reprezentowana przez plik w systemie plików) będzie działała na zdalnym systemie plików?
Czy kolejka nazwana FIFO (reprezentowana przez plik w systemie plików) będzie działała na zdalnym systemie plików?
Czy komenda fork tworzy proces czy też wątek?
Czy komenda fork tworzy proces czy też wątek?
Czy proces w trybie systemu może zażądać dostępu do dowolnego adresu pamięci?
Czy proces w trybie użytkownika może zażądać dostępu do dowolnego adresu pamięci?
Czy proces w trybie użytkownika może zażądać dostępu do dowolnego adresu? Uzasadnij dlaczego?
Czym zajmuje się planista (scheduler)?
Czym zajmuje się planista (scheduler)?
Do czego może służyć semafor?
Do czego może służyć semafor?
Do czego można wykorzystać obsługę sygnałów?
Do czego można wykorzystać obsługę sygnałów?
Do czego służą sygnały?
Do czego służą sygnały?
Do czego służy gniazdo sieciowe?
Do czego służy gniazdo sieciowe?
Do czego służy licznik otwarć pliku?
Do czego służy licznik otwarć pliku?
Do czego służy pamięć wirtualna?
Do czego służy pamięć wirtualna?
Do czego służy program make?
Do czego służy program make?
Do czego służy rejestr graniczny?
Do czego służy rejestr graniczny? 
Do czego służy sekcja krytyczna?
Do czego służy sekcja krytyczna?
Jak można zrealizować stronicowanie na żądanie?
Jak można zrealizować stronicowanie na żądanie?
Jak rozumiesz pojęcia pamięci wirtualnej?
Jak rozumiesz pojęcia pamięci wirtualnej?
Jak się ma program do procesu?
Jak się ma program do procesu?
Jak się ma termin ,,obliczenia równoległe'' do ,,obliczeń współbieżnych''?
Jak się ma termin ,,obliczenia równoległe'' do ,,obliczeń współbieżnych''?
Jak wygląda schemat producent - konsument?
Jak wygląda schemat producent - konsument?
Jaka jednostka w komputerze służy do tłumaczenia adresów logicznych na fizyczne?
Jaka jednostka w komputerze służy do tłumaczenia adresów logicznych na fizyczne?
Jaka jest różnica miedzy ścieżką relatywną, a absolutną? Podaj także przykład.
Jaka jest różnica miedzy ścieżką relatywną, a absolutną? Podaj także przykład.
Jaka jest różnica między gniazdem datagramowym a strumieniowym?
Jaka jest różnica między tablicą statyczną a dynamiczną (w kontekście języka C)?
Jaka jest różnica między tablicą statyczną a dynamiczną (w kontekście języka C)?
Jaki mechanizm pozwala na tworzenie połączeń przez sieć komputerową?
Jaki mechanizm sprzętowy pozwala na przełączanie kontekstu?
Jaki typ plików jest bezpośrednio obsługiwany przez praktycznie każdy system operacyjny?
Jaki typ plików jest bezpośrednio obsługiwany przez praktycznie każdy system operacyjny?
Jaki znasz system plików, który nie pozwala na cykle?
Jakie mechanizmy sprzętowe i programowe umożliwiają obsługę pamięci wirtualnej?
Jakie mechanizmy sprzętowe i programowe umożliwiają obsługę pamięci wirtualnej?
Jakie są etapy prowadzące od kodu źródłowego w C do programu wykonywalnego?
Jakie są etapy prowadzące od kodu źródłowego w C do programu wykonywalnego?
Jakie znasz 2 podstawowe rodzaje gniazd w kontekście utrzymania połączenia?
Jakie znasz 2 podstawowe rodzaje gniazd w kontekście utrzymania połączenia?
Jakie znasz dwa tryby działania procesora (w kontekście uprawnień i systemów operacyjnych)?
Jakie znasz dwa tryby działania procesora (w kontekście uprawnień i systemów operacyjnych)?
Kiedy może następować wiązanie adresów?
Kiedy może następować wiązanie adresów?
Na czym polega kopiowanie przy zapisie?
Na czym polega kopiowanie przy zapisie?
Na czym polega ,,problem czytelników i pisarzy''?
Na czym polega ,,problem czytelników i pisarzy''?
Na czym polega przełączanie kontekstu?
Na czym polega przełączanie kontekstu?
Opisz co się stanie, jeśli w problemie jedzących filozofów każdy filozof zechce jeść i podniesie najpierw lewą pałeczkę?
Opisz co się stanie, jeśli w problemie jedzących filozofów każdy filozof zechce jeść i podniesie najpierw lewą pałeczkę?
Opisz co się stanie, jeśli w problemie jedzących filozofów każdy filozof zechce jeść i podniesie najpierw prawą pałeczkę?
Opisz co się stanie, jeśli w problemie jedzących filozofów każdy filozof zechce jeść i podniesie najpierw prawą pałeczkę?
Opisz jak wygląda problem jedzących filozofów?
Opisz jak wygląda problem jedzących filozofów?
Przez jakie stany przechodzi proces?
Przez jakie stany przechodzi proces?
W jaki sposób można zrealizować sekcję mutex?
W jaki sposób można zrealizować sekcję mutex?
W jaki sposób rozwiązuje się problem sytuacji wyścigu?
W jaki sposób rozwiązuje się problem sytuacji wyścigu?
W jakich sytuacjach program uruchomiony w systemie operacyjnym korzysta z adresów fizycznych?
W jakich sytuacjach program uruchomiony w systemie operacyjnym korzysta z adresów fizycznych?
Wyjaśnij (na jakimś przykładzie) sytuację wyścigu.
Wyjaśnij (na jakimś przykładzie) sytuację wyścigu. 
Wymień jakie znasz metody komunikacji międzyprocesowej.
Wymień jakie znasz metody komunikacji międzyprocesowej.
Z czego się składa system komputerowy?
Z czego się składa system komputerowy?
Z jakiego powodu stosuje się biblioteki współdzielone?
Z jakiego powodu stosuje się biblioteki współdzielone?
Z jakiego powodu system operacyjny jest przydatny? Co umożliwia?
Z jakiego powodu system operacyjny jest przydatny? Co umożliwia?
