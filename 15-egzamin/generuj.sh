#!/bin/bash

ZADAN=14
RZEDOW=90



NMAX=`wc -l zadania.txt | awk '{print $1}'`


echo '
\documentclass[10pt]{tpexercise}
\usepackage[utf8]{inputenc}

\setPL

\doTest
%\doLab
\tytul{SOP Egzamin. ~~~ czas: 1:00}

\begin{document}
'



for rzad in `seq 1 $RZEDOW`; do


ORDERFILE=/tmp/order
echo > $ORDERFILE
for i in `seq 1 $NMAX`; do
	echo $RANDOM $i >> $ORDERFILE
done

sort -n $ORDERFILE > $ORDERFILE.l
ORDER=`cat $ORDERFILE.l | awk '/.+ .+/{print $2}' | head -n $ZADAN`



echo '
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\naglowek{'$rzad'}


Każde pytanie za 1 pkt. Odpowiedzi proszę tworzyć jak najkrótsze.\\

'

for i in $ORDER; do
	echo "\\zadanie $(cat zadania.txt | head -n $i | tail -n 1)"
	echo " \\\\"
	echo " \\\\"
	echo " \\\\"
done

echo '

'
rm -f $ORDERFILE.l $ORDERFILE

done

echo '
\end{document}
'



